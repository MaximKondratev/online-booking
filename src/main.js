import Vue from 'vue'
import App from './App.vue'
import store from './store'
import router from './router';
const VueInputMaks = require('vue-inputmask').default;

Vue.use(VueInputMaks)
Vue.config.productionTip = false

new Vue({
  store,
  router,
  render: h => h(App)
}).$mount('#app')
