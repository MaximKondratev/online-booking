import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        cities: [],

        city: '',
        cityInfo: {},

        selectedDay: '',
        selectedTime: '',
        phone: '',
        name: '',

        nameValid: true,
        nameFilled: false,
        phoneValid: true,
        dayValid: true,
        timeValid: true,
        
        valid: false,

        active: true,
        loading: false,

        showModal: false,

        tabHandler: function (event) {
            if(event.keyCode == 9){
                event.preventDefault();
                return;
            }
        }
    },
    mutations: {
        setDay(state, payload) {
            state.selectedDay = payload;
        },
        setTime(state, payload) {
            state.selectedTime = payload;
        },
        resetData(state){
            state.cityInfo = '';
            state.selectedDay= '';
            state.selectedTime= '';
        },
        setLoading(state, payload) {
            state.loading = payload;
            state.active = payload;
        },
        setName(state, payload) {
            state.name = payload;
        },
        setPhone(state, payload) {
            state.phone = payload;
        },
        isDayValid(state, payload) {
            state.dayValid = payload;
        },
        isTimeValid(state, payload) {
            state.timeValid = payload;
        },
        setNameValid(state, payload) {
            state.nameValid = payload;
        },
        setNameFilled(state, payload) {
            state.nameFilled = payload;
        },
        setPhoneValid(state, payload) {
            state.phoneValid = payload;
        },
        showModal(state, payload) {
            if (payload == true)
                document.body.classList.add('hidden');
            else {
                document.body.classList.remove('hidden');
                document.getElementById('app').removeEventListener('keydown', state.tabHandler);
            }
            state.showModal = payload;
        }
        
    },
    actions: {
        getCities({state, commit}){
            commit('setLoading', true);
            state.active = true;
            var config = {
                headers: {'Access-Control-Allow-Origin': '*'}
            };
            axios.get('https://www.mocky.io/v2/5b34c0d82f00007400376066?mocky-delay=700ms', config)
            .then( response => {
                state.cities = response.data.cities;
                commit('setLoading', false);
                state.active = false;
            })
        },

        setDefaultCity({state, commit}) {
            commit('setLoading', true);
            state.active = true;
            let defaultCity = state.cities.find( (el) => {
                return el.name == 'Владивосток'
            });
            if (defaultCity !== undefined){
                let id = defaultCity.id;
                var config = {
                    headers: {'Access-Control-Allow-Origin': '*'}
                };
                axios.get(`https://www.mocky.io/v2/${id}?mocky-delay=700ms`, config)
                .then( (res) => {
                    //Vue.set(state.cityInfo, res.data.data )
                     state.cityInfo = res.data
                     commit('setLoading', false);
                     state.active = false;

                });
            }
            state.city = defaultCity === undefined ? '' : defaultCity;
            

        },

        setCity({state, commit}, city) {
            commit('setLoading', true);
            state.active = true;
            state.city = city;
            state.day = '';
            state.time = '';
            state.dayValid = true;
            state.timeValid= true;
            commit('resetData');
            axios.get(`https://www.mocky.io/v2/${city.id}?mocky-delay=700ms`)
            .then( (res) => {
                if (res.data.success){
                    state.cityInfo = res.data;
                    commit('setLoading', false);
                    state.active = false;

                }
            });
        },

        checkForm({state}) {
            if ((state.selectedDay &&
                state.selectedTime &&
                state.phone &&
                state.name) &&
                state.nameValid &&
                state.phoneValid &&
                state.dayValid &&
                state.timeValid)
                state.valid = true;
            else
                state.valid = false;
        },

        submitForm({state}) {
            document.getElementById('app').addEventListener('keydown', state.tabHandler);
            let data = {
                city: state.city,
                // cityInfo: state.cityInfo,
                day: state.selectedDay.day,
                time: state.selectedTime,
                phone: state.phone,
                name: state.name
            }
            // console.log(data);
            let key = 'order-' + data.city.name + ' ' + data.day + ' ' + data.time;
            localStorage.setItem(key, JSON.stringify(data));
        },

        resetForm({state}) {
            state.selectedDay = ''
            state.selectedTime = ''
            state.phone = ''
            state.name = ''
            
            state.nameValid = true
            state.nameFilled = false;
            state.phoneValid = true
            state.dayValid = true
            state.timeValid = true
            
            state.valid = false
        }

    }
})
